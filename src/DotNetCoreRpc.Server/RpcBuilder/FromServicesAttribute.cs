﻿using System;

namespace DotNetCoreRpc.Server.RpcBuilder
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FromServicesAttribute : Attribute
    {

    }
}
